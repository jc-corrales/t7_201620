package taller.interfaz;

import sun.misc.Queue;

public class ArbolBinario <K extends Comparable <K>, V>
{
	private NodoArbolBinario raiz;
	
	private class NodoArbolBinario
	{
		private K llave;
		private V valor;
		private NodoArbolBinario izquierdo;
		private NodoArbolBinario derecho;
		private int N;
		
		public NodoArbolBinario(K pLlave, V pValor, int pN)
		{
			this.llave = pLlave;
			this.valor = pValor;
			this.N = pN;
		}

	}
	
	public int size()
	{
		return size(raiz);
	}
	
	private int size(NodoArbolBinario x)
	{
		if(x == null)
		{
			return 0;
		}
		else
		{
			return x.N;
		}
	}
	
	public V get(K pLlave)
	{
		return get(raiz, pLlave);
	}
	
	private V get(NodoArbolBinario x, K pLlave)
	{
		if(x == null)
		{
			return null;
		}
		int cmp = pLlave.compareTo(x.llave);
		if(cmp < 0)
		{
			return get(x.izquierdo, pLlave);
		}
		else if(cmp > 0)
		{
			return get (x.derecho, pLlave);
		}
		else
		{
			return x.valor;
		}
	}
	
	public void put(K pLlave, V pValor)
	{
		raiz = put(raiz, pLlave, pValor);
	}
	
	private NodoArbolBinario put(NodoArbolBinario entrada, K pLlave, V pValor)
	{
		if(entrada == null)
		{
			return new NodoArbolBinario(pLlave, pValor, 1);
		}
		int cmp = pLlave.compareTo(entrada.llave);
		if(cmp < 0)
		{
			entrada.izquierdo = put(entrada.izquierdo, pLlave, pValor);
		}
		else if(cmp > 0)
		{
			entrada.derecho = put (entrada.derecho, pLlave, pValor);
		}
		else
		{
			entrada.valor = pValor;
		}
		entrada.N = size(entrada.izquierdo) + size(entrada.derecho) + 1;
		return entrada;
	}
	
	public K min()
	{
		return min(raiz).llave;
	}
	
	private NodoArbolBinario min(NodoArbolBinario criterio)
	{
		if(criterio.izquierdo == null)
		{
			return criterio;
		}
		return min(criterio.izquierdo);
	}
	
	public K max()
	{
		return max(raiz).llave;
	}
	
	private NodoArbolBinario max(NodoArbolBinario criterio)
	{
		if(criterio.derecho == null)
		{
			return criterio;
		}
		return min(criterio.derecho);
	}
	
	public K floor(K pLlave)
	{
		NodoArbolBinario x = floor(raiz, pLlave);
		if(x == null)
		{
			return null;
		}
		return x.llave;
	}
	
	private NodoArbolBinario floor(NodoArbolBinario x, K pLlave)
	{
		if(x == null)
		{
			return null;
		}
		int cmp = pLlave.compareTo(x.llave);
		if(cmp == 0)
		{
			return x;
		}
		if(cmp < 0)
		{
			return floor(x.izquierdo, pLlave);
		}
		NodoArbolBinario t = floor(x.derecho, pLlave);
		if( t != null)
		{
			return t;
		}
		else
		{
			return x;
		}
	}
	
	public K ceiling(K pLlave)
	{
		NodoArbolBinario x = ceiling(raiz, pLlave);
		if(x == null)
		{
			return null;
		}
		return x.llave;
	}
	
	private NodoArbolBinario ceiling(NodoArbolBinario x, K pLlave)
	{
		if(x == null)
		{
			return null;
		}
		int cmp = pLlave.compareTo(x.llave);
		if(cmp == 0)
		{
			return x;
		}
		if(cmp > 0)
		{
			return floor(x.derecho, pLlave);
		}
		NodoArbolBinario t = floor(x.izquierdo, pLlave);
		if( t != null)
		{
			return t;
		}
		else
		{
			return x;
		}
	}
	
	public K select(int k)
	{
		return select(raiz, k).llave;
	}
	
	private NodoArbolBinario select(NodoArbolBinario x, int k)
	{
		if(x == null)
		{
			return null;
		}
		int t = size(x.izquierdo);
		if(t > k)
		{
			return select(x.izquierdo, k);
		}
		else if(t < k)
		{
			return select(x.derecho, k-t-1);
		}
		else
		{
			return x;
		}
	}
	
	public int rank(K pLlave)
	{
		return rank(pLlave, raiz);
	}
	
	private int rank(K pLlave, NodoArbolBinario x)
	{
		if(x == null)
		{
			return 0;
		}
		int cmp = pLlave.compareTo(x.llave);
		if(cmp < 0)
		{
			return rank(pLlave, x.izquierdo);
		}
		else if(cmp > 0)
		{
			return (1 + size(x.izquierdo) + rank(pLlave, x.derecho));
		}
		else
		{
			return size(x.izquierdo);
		}
	}
	
	
	public void deleteMin()
	{
		raiz = deleteMin(raiz);
	}
	
	private NodoArbolBinario deleteMin(NodoArbolBinario x)
	{
		if(x.izquierdo == null)
		{
			return x.derecho;
		}
		x.izquierdo = deleteMin(x.izquierdo);
		x.N = (size(x.izquierdo) + size(x.derecho) + 1);
		return x;
	}
	
	public void delete(K pLlave)
	{
		raiz = delete(raiz, pLlave);
	}
	
	private NodoArbolBinario delete(NodoArbolBinario x, K pLlave)
	{
		if(x == null)
		{
			return null;
		}
		int cmp = pLlave.compareTo(x.llave);
		if(cmp < 0)
		{
			x.izquierdo = delete(x.izquierdo, pLlave);
		}
		else if(cmp > 0)
		{
			x.derecho = delete(x.derecho, pLlave);
		}
		else
		{
			if(x.derecho == null)
			{
				return x.izquierdo;
			}
			if(x.izquierdo == null)
			{
				return x.derecho;
			}
			NodoArbolBinario t = x;
			x = min(t.derecho);
			x.derecho = deleteMin(t.derecho);
			x.izquierdo = t.izquierdo;
		}
		x.N = (size(x.izquierdo) + size(x.derecho) + 1);
		return x;
	}
	
	public void deleteMax()
	{
		raiz = deleteMax(raiz);
	}
	
	private NodoArbolBinario deleteMax(NodoArbolBinario x)
	{
		if(x.derecho == null)
		{
			return x.izquierdo;
		}
		x.derecho = deleteMax(x.derecho);
		x.N = (size(x.derecho) + size(x.izquierdo) + 1);
		return x;
	}
	
	public Iterable<K> llaves()
	{
		return llaves(min(), max());
	}
	
	@SuppressWarnings("unchecked")
	public Iterable<K> llaves(K lo, K hi)
	{
		Queue<K> cola = new Queue<K>();
		llaves(raiz, cola, lo, hi);
		return (Iterable<K>) cola;
	}
	
	private void llaves(NodoArbolBinario x, Queue<K> cola, K lo, K hi)
	{
		if(x == null)
		{
			return;
		}
		int cmplo = lo.compareTo(x.llave);
		int cmphi = hi.compareTo(x.llave);
		if(cmplo < 0)
		{
			llaves(x.izquierdo, cola, lo, hi);
		}
		if((cmplo <= 0) && (cmphi >= 0))
		{
			cola.enqueue(x.llave);
		}
		if(cmphi >= 0)
		{
			llaves(x.derecho, cola, lo, hi);
		}
	}
	
	private void preOrden(NodoArbolBinario elem)
	{
		if(elem == null)
		{
			return;
		}
		System.out.println(elem.valor + ";");
		preOrden(raiz.izquierdo);
		preOrden(raiz.derecho);
	}
	
	private void inOrden(NodoArbolBinario elem)
	{
		if(elem == null)
		{
			return;
		}
		inOrden(elem.izquierdo);
		System.out.println(elem.valor);
		inOrden(elem.derecho);
	}
	
	private void postOrden(NodoArbolBinario elem)
	{
		if(elem == null)
		{
			return;
		}
		postOrden(elem.izquierdo);
		postOrden(elem.derecho);
		System.out.println(elem.valor);
	}
	
	public void preOrden()
	{
		preOrden(raiz);
	}
	
	public void inOrden()
	{
		inOrden(raiz);
	}
	
	public void posOrden()
	{
		postOrden(raiz);
	}
}