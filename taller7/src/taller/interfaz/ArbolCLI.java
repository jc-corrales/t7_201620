package taller.interfaz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.*;;


public class ArbolCLI implements IReconstructorArbol
{

	private Scanner in;

	private ArrayList<String> preOrden;
	private ArrayList<String> inOrden;
	private ArbolBinario<String, String> arbol;

	public ArbolCLI()
	{
		in = new Scanner(System.in);
		preOrden = new ArrayList<String>();
		inOrden = new ArrayList<String>();
	}

	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{	
			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();
			switch(opt1)
			{
			case 1:
				recibirArchivo();
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");
			String entrada = in.next();
			// TODO Leer el archivo .properties 

			try
			{
				cargarArchivo(entrada);
			}
			catch(Exception e)
			{
				System.out.println("Error en la generación del árbol: " + e.getMessage());
				e.printStackTrace();
			}
			try {

				// TODO cargarArchivo
				// TODO Reconstruir árbol  
				reconstruir();

				String nombre = "/data/arbolPlantado.json";

				crearArchivo(nombre);

				System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");
				System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");
				System.out.println("Presione 1 para salir");
				in.next();
				finish=true;

			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}

	@Override
	public void cargarArchivo(String nombre) throws IOException
	{
		arbol = new ArbolBinario<String, String>();
		//		File file = new File(nombre);
		// TODO Auto-generated method stub
		//		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		//		BufferedWriter bw = new BufferedWriter(fw);

		FileReader fr = new FileReader(nombre);
		BufferedReader br = new BufferedReader(fr);

//		System.out.println(br.read());

		String elem1 = br.readLine();
		System.out.println(elem1);
		String elem2 = br.readLine();
		System.out.println(elem2);

		String[] elem11 = elem1.split("=");
		String[] elem22 = elem2.split("=");

		String[] elem111 = elem11[1].split(",");
		String[] elem222 = elem22[1].split(",");

		for(int i = 0; i < elem111.length; i++)
		{
			preOrden.add(elem111[i]);
		}
		for(int j = 0; j < elem222.length; j++)
		{
			inOrden.add(elem222[j]);
		}

		//		bw.write("");
		//		bw.newLine();


		//		fw.close();
		fr.close();
		br.close();
		//		bw.close();
	}

	@Override
	public void crearArchivo(String info) throws FileNotFoundException,
	UnsupportedEncodingException
	{
		//		try
		//		{
		//			File file = new File(info);
		//			if (!file.exists())
		//			{
		//				file.createNewFile();
		//			}
		//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
		//			BufferedWriter bw = new BufferedWriter(fw);
		//			bw.write("[");
		//			bw.newLine();
		//			bw.write("{");
		//			bw.newLine();
		//
		//
		//
		//			for(int i = 0; i < arbol.size(); i++)
		//			{
		//				if(i < arbol.size())
		//				{
		//					String temp = arbol.get("Nodo"+i);
		//					String respuesta = "Nodo"+i + ":" + temp + ",";
		//					bw.write(respuesta);
		//					bw.newLine();
		//				}
		//				else
		//				{
		//					String temp = arbol.get("Nodo"+i);
		//					String respuesta = "Nodo"+i + ":" + temp;
		//					bw.write(respuesta);
		//					bw.newLine();
		//				}
		//			}
		//			bw.write("}");
		//			bw.write("]");
		//			bw.close();
		//			fw.close();
		//		}
		//		catch(Exception e)
		//		{
		//			e.printStackTrace();
		//		}
		JSONObject obj = new JSONObject();
		JSONArray list = new JSONArray();


		for(int i = 0; i < arbol.size(); i++)
		{
			if(i < arbol.size())
			{
				list.add(i, arbol.get("Nodo"+i));
			}
			else
			{
			}
		}

		obj.put("Arbol", list);

		try {

			FileWriter file = new FileWriter(info);
			file.write(obj.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(obj);
		// TODO Auto-generated method stub

	}

	@Override
	public void reconstruir()
	{
		ArbolBinario<String, String> temp1 = new ArbolBinario<String, String>();
		ArbolBinario<String, String> temp2 = new ArbolBinario<String, String>();
		for(int i = 0; i < preOrden.size(); i++)
		{
			String elem = preOrden.get(i);
			temp1.put("Nodo" + i, elem);
		}
		// TODO Auto-generated method stub
		for(int i = 0; i < inOrden.size(); i++)
		{
			String elem = inOrden.get(i);
			temp2.put("Nodo" + i, elem);
		}
		temp1.inOrden();
		temp2.posOrden();
	}
} 